# README #

### What is this repository for? ###

This repository contains a Latex template that might be used to write PSR's reports and software manuals.

### How do I use it? ###

You should download at least four files: (i)'psr2latex.cls' (ii)'PSRFrontpage.png' (ii)'bibliography.bib' and (iv)'Template.tex' and save all of them in the same folder.

You should work on the latter two files. You can add the references of your work in file (iii). You'll really work on file (iv). This one you may want to (and should) rename.

I also wrote a guide for the new Latex users, with some useful tips and links. There you can find standart layouts for using tables, figures and algorithms. You can download all the necessary files from the 'Full Template.rar'.

### Who do I talk to? ###
Rodrigo de Mello Novaes
rnovaes@psr-inc.com